Feature: Sample Feature to Demonstrate Utility Methods

@SampleTag
Scenario: Hit Api
    Given url 'http://example.com'
    When method get
    Then status 200

Scenario: Hit Api After Setting Up Basic Authentication
    * def authUtility = Java.type('karate.junit.AuthUtility')
    * header Authorization = authUtility.basicAuthEncoding('username', 'password')
    Given url 'http://example.com'
    When method get
    Then status 200

Scenario: Retrieve Command Line Argument
    * def commandLineUtility = Java.type('karate.junit.CommandLineUtility')
    * def sample = commandLineUtility.getArg('sample')
    * match sample == "demo"

Scenario: Get Sample Date Time Formats
    * def dateTimeUtility = Java.type('karate.junit.DateTimeUtility')
    * dateTimeUtility.threadSleep(3000)
    * print dateTimeUtility.getFormattedDate()
    * print dateTimeUtility.getFormattedTime()
    * print dateTimeUtility.getFormattedDateTime()
    * print dateTimeUtility.getReportFormattedDateTime()
    * print dateTimeUtility.getCurrentTimeInMilliseconds()

Scenario: Get Sample Number and UUID
    * def randomGeneratorUtility = Java.type('karate.junit.RandomGeneratorUtility')
    * print randomGeneratorUtility.generateRandomNumber(300)
    * print randomGeneratorUtility.generateUUID()