package karate.junit;

import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import karate.junit.App;

public class AppTest {
    @Test public void testSomeLibraryMethod() {
        App classUnderTest = new App();
        assertNotNull(classUnderTest);
    }

    @Test
    public void testMain() {
        App.main(new String[] {"arg"});
    }
}